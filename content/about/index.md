+++
title = "About"
description = "My trade is software engineering, my passion is music and blockchain, and my forte is solving people's technical problems."
author = "TechDadNomad aka @tuckcodes"
date = "2020-09-07"
layout = "about"
+++

## Tuck

I'm an autodidact coder, DevOps junkie, <a href="../categories/blockchain/">blockchain</a> nutjob, <a href="../categories/music/">musical</a> gypsy, blessed <a href="../categories/fatherhood/">dad</a>, happy husband, and lover of life. I've toured far and wide using six strings, lived on boats, islands, in RVs, and cars. I've been exalted and I've been humbled. I seek to make the world a better place by being a <a href="../categories/philosophy/">better person</a>.

## The site

This site is built with Hugo, hosted with Gitlab Pages. I make changes to the code locally and push those changes to my Gitlab repository. Once those changes make it to Gitlab it triggers my pipeline, which verified that nothing is broken with my Hugo setup. Once the testing is finished in my pipeline, the final job in my Gitlab pipeline will publish my changes to Gitlab Pages.

> The majority of what I have can be setup within seconds via <a href="https://gitlab.com/pages/hugo" target="_blank">this Gitlab template</a>

